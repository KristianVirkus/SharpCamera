﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  A CameraWidget that has multiple choices to it.

using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;

namespace libgphoto2Sharp
{
    public static partial class GPhoto2
    {
        #region P/Invoke Functions
        [DllImport(GPhoto2DLL, CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
        private static extern int gp_widget_add_choice(
            IntPtr widget,
            [MarshalAs(UnmanagedType.LPStr)] string choice
        );

        [DllImport(GPhoto2DLL)]
        private static extern int gp_widget_count_choices(IntPtr widget);

        [DllImport(GPhoto2DLL)]
        private static extern int gp_widget_get_choice(IntPtr widget, int choice_number, out IntPtr choice);
        #endregion


        /// <summary>
        /// A CameraWidget that has multiple selections.
        ///
        /// This is more of a base class for the RadioCameraWidget and the
        /// MenuCameraWidget.  I don't recommend instantiating this one.  It's mainly here for the purposes of writing
        /// this wrapper: code reuse and flexability.
        /// </summary>
        public class ChoicesCameraWidget : CameraWidget
        {
            /// <summary>
            /// Create a Camera Widget that has multiple choices.
            /// </summary>
            /// <param name="widget">handle to the widget, needs to be a Radio or Menu Widget</param>
            internal ChoicesCameraWidget(IntPtr widget)
                : base(widget)
            {
                if ((Type != CameraWidgetType.GP_WIDGET_RADIO) && (Type != CameraWidgetType.GP_WIDGET_MENU))
                    throw new ArgumentException("supplied handle is not for a Radio or Menu widget");
            }

            internal ChoicesCameraWidget() { }

            /// <summary>
            /// Adds a choice to the widget
            /// </summary>
            /// <param name="choice">Choice to add</param>
            public void AddChoice(string choice)
            {
                int ok = gp_widget_add_choice(handle, choice);
                if (ok != GP_OK)
                    throw new GPhoto2Exception("Error adding choice", ok);
            }

            /// <summary>
            /// Count the choices of the widget.
            /// </summary>
            /// <value>The number of choices available.</value>
            public int CountChoices
            {
                get
                {
                    int num = gp_widget_count_choices(handle);
                    if (num < GP_OK)
                        throw new GPhoto2Exception("Error counting the widget choices", num);
                    return num;
                }
            }

            /// <summary>
            /// Get a single choice from the widget.
            /// </summary>
            /// <returns>The choice.</returns>
            /// <param name="choiceNumber">Choice number.</param>
            public string GetChoice(int choiceNumber)
            {
                // Check range
                int maxChoices = CountChoices;
                if ((choiceNumber < 0) || (choiceNumber >= maxChoices))
                    throw new IndexOutOfRangeException("Bad index provided");

                // It's good, try to get the choice
                IntPtr choicePtr;
                int ok = gp_widget_get_choice(handle, choiceNumber, out choicePtr);
                if (ok != GP_OK)
                    throw new GPhoto2Exception("Error getting widget chioce", ok);

                return Marshal.PtrToStringAnsi(choicePtr);
            }

            /// <summary>
            /// Get all of the choices available for the widget
            /// </summary>
            /// <value>The choices.</value>
            public List<string> Choices
            {
                get
                {
                    int total = CountChoices;
                    List<string> choices = new List<string>(total);
                    for (int i = 0; i < total; i++)
                        choices.Add(GetChoice(i));

                    return choices;
                }
            }

            /// <summary>
            /// Get/Set the value of the Radio widget.  Not that this will not change value of the widget on the camera
            /// itself until this widget has been passed into Camera.SetSingleConfig.
            /// </summary>
            /// <value>One of the possible chioces from `Choices`</value>
            public string Value
            {
                get
                {
                    IntPtr valuePtr = IntPtr.Zero;
                    int ok = gp_widget_get_value(handle, out valuePtr);
                    if (ok != GP_OK)
                        throw new GPhoto2Exception("Error getting widget choice", ok);

                    return Marshal.PtrToStringAnsi(valuePtr);
                }

                set
                {
                    // Make sure it's good first
                    if (!Choices.Contains(value))
                        throw new ArgumentException("Provided value is not a valid choice for the widget", value);

                    // Try to set it
                    IntPtr valueStr = Marshal.StringToHGlobalAnsi(value);
                    int ok = gp_widget_set_value(handle, valueStr);
                    if (ok != GP_OK)
                        throw new GPhoto2Exception("Error setting widget choice ", ok);

                    // Cleanup
                    Marshal.FreeHGlobal(valueStr);
                }
            }
        }
    }
}

