﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  Camera operations.

using System;

namespace libgphoto2Sharp
{
    public static partial class GPhoto2
    {
        public enum CameraOperation
        {
            GP_OPERATION_NONE               = 0,        // No remote control operation supported.
            GP_OPERATION_CAPTURE_IMAGE      = 1 << 0,   // Capturing images supported.
            GP_OPERATION_CAPTURE_VIDEO      = 1 << 1,   // Capturing videos supported.
            GP_OPERATION_CAPTURE_AUDIO      = 1 << 2,   // Capturing audio supported.
            GP_OPERATION_CAPTURE_PREVIEW    = 1 << 3,   // Capturing image previews supported.
            GP_OPERATION_CONFIG             = 1 << 4,   // Camera and Driver configuration supported.
            GP_OPERATION_TRIGGER_CAPTURE    = 1 << 5    // Camera can trigger capture and wait for events.
        }
    }
}

