﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  Device types.

using System;

namespace libgphoto2Sharp
{
    public static partial class GPhoto2
    {
        public enum GphotoDeviceType
        {
            GP_DEVICE_STILL_CAMERA = 0,         // Traditional still camera
            GP_DEVICE_AUDIO_PLAYER = 1 << 0     // Audio player
        }
    }
}

