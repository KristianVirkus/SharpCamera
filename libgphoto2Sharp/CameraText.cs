﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  Camera text.

using System;
using System.Runtime.InteropServices;

namespace libgphoto2Sharp
{
    public static partial class GPhoto2
    {
        [StructLayout(LayoutKind.Sequential, CharSet=CharSet.Ansi)]
        public struct CameraText
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst=32 * 1024)]
            public sbyte[] text;

            #region C# Friendly Properties
            public String Text
            {
                get { return Utils.SByteArrayToString(text); }
            }
            #endregion
        }
    }
}

