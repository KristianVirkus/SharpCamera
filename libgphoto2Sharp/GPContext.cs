﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  Camera context.

using System;
using System.Runtime.InteropServices;

namespace libgphoto2Sharp
{
    public static partial class GPhoto2
    {
        #region P/Invoke functions
        [DllImport(GPhoto2DLL)]
        private static extern IntPtr gp_context_new();

        [DllImport(GPhoto2DLL)]
        private static extern void gp_context_ref(IntPtr context);

        [DllImport(GPhoto2DLL)]
        private static extern void gp_context_unref(IntPtr context);
        #endregion


        public class GPContext : IDisposable {
            private bool disposed = false;
            public IntPtr handle;    // GPContext *

            public GPContext() {
                handle = gp_context_new();
            }

            ~GPContext() {
                Dispose(false);
            }

            public void Dispose() {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            /// Does the actual cleanup work
            protected virtual void Dispose(bool disposing)
            {
                if (disposed)
                    return;

                // Free Managed resources
                if (disposing)
                {
                }

                // Free unmanged resources
                gp_context_unref(handle);

                disposed = true;
            }
        }
    }
}

