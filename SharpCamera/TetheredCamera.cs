﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  A camera that's tethered.  You can control it.

using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using libgphoto2Sharp;

namespace SharpCamera
{
    /// <summary>
    /// A C# interface to libGPhoto2 backed cameras, that can be controled.
    ///
    /// Includes some methods for setting configurations on the camera and getting images.
    ///
    /// NOTE: For now, this only really is able to get JPEG images from the camera.
    /// </summary>
    public class TetheredCamera
    {
        private GPhoto2.Camera camera;
        private GPhoto2.RadioCameraWidget isoWidget = null;
        private GPhoto2.RadioCameraWidget apertureWidget = null;
        private GPhoto2.RadioCameraWidget shutterSpeedWidget = null;
        private string name;
        private string usbPort;
        private bool connectionEstablished = false;

        /// <summary>
        /// The root configuration widget of the camera.
        /// </summary>
        public GPhoto2.WindowCameraWidget rootWidget = null;

        /// <summary>
        /// How long the camera should wait in the `Connected` property.
        /// </summary>
        /// <value>Time is in milliseconds; ;please use a positive number.  Default is 10 ms.</value>
        public uint HeartbeatTimeout { get; set; } = 10;

        #region Properties
        /// <summary>
        /// Model Name of the camera
        /// </summary>
        /// <value>The name.</value>
        public string Name
        {
            get { return name; }
        }

        /// <summary>
        /// USB Port the camera is connected on.
        /// </summary>
        /// <value>The USB port.</value>
        public string USBPort
        {
            get { return usbPort; }
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="SharpCamera.TetheredCamera"/> connection established.
        /// </summary>
        /// <value><c>true</c> if connection established; otherwise, <c>false</c>.</value>
        public bool ConnectionEstablished
        {
            get { return connectionEstablished; }
        }
        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="SharpCamera.TetheredCamera"/> class.
        ///
        /// It's not recommended to use this constructor, unless you know what you're doing.  Check the static `Scan()`
        /// and `GetFirst()` methods instead; they are much friendlier to use.
        /// </summary>
        /// <param name="name">The model name of the camera..</param>
        /// <param name="usbPort">Usb port.</param>
        /// <param name="portList">Port list.</param>
        public TetheredCamera(string name, string usbPort, GPhoto2.GPPortInfoList portList)
        {
            this.name = name;
            this.usbPort = usbPort;
            camera = new GPhoto2.Camera();

            // Set the port informatoin
            int portNum = portList.LookupPath(USBPort);
            GPhoto2.GPPortInfo portInfo = portList.GetInfo(portNum);
            camera.PortInfo = portInfo;
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the
        /// <see cref="SharpCamera.TetheredCamera"/> is reclaimed by garbage collection.
        ///
        /// Will also close the connection to the camera.
        /// </summary>
        ~TetheredCamera()
        {
            Exit();
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to the current <see cref="SharpCamera.TetheredCamera"/>.
        ///
        /// It's possible that this object and the other one may be different in memory, but this checks to see if they
        /// are equivalent.  The metric is the camera name and which USB port they're on.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object"/> to compare with the current <see cref="SharpCamera.TetheredCamera"/>.</param>
        /// <returns><c>true</c> if the specified <see cref="System.Object"/> is equal to the current
        /// <see cref="SharpCamera.TetheredCamera"/>; otherwise, <c>false</c>.</returns>
        public override bool Equals(object obj)
        {
            // Check for null values and compare run-time types.
            if (obj == null || GetType() != obj.GetType())
                return false;

            // See if values are the same or not
            TetheredCamera other = (TetheredCamera)obj;
            return (Name == other.Name) && (USBPort == other.USBPort);
        }

        /// <summary>
        /// Serves as a hash function for a <see cref="SharpCamera.TetheredCamera"/> object.
        ///
        /// Uses it's `Name` and `USBPort`, XOR'd for a hash.
        /// </summary>
        /// <returns>A hash code for this instance that is suitable for use in hashing algorithms and data structures such as a
        /// hash table.</returns>
        public override int GetHashCode()
        {
            return Name.GetHashCode() ^ USBPort.GetHashCode();
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents the current <see cref="SharpCamera.TetheredCamera"/>.
        /// </summary>
        /// <returns>A <see cref="System.String"/> that represents the current <see cref="SharpCamera.TetheredCamera"/>.</returns>
        public override string ToString()
        {
            return string.Format("{0} on USB Port {1}", Name, USBPort);
        }

        /// <summary>
        /// Turns on the connection to the camera
        /// </summary>
        public void Connect()
        {
            // Establish connection oce
            if (connectionEstablished)
                return;

            camera.Init();
            connectionEstablished = true;
            // Try to get the common widgets
            try
            {
                isoWidget = (GPhoto2.RadioCameraWidget)camera.GetSingleConfig("iso");
            }
            catch (GPhoto2.WidgetNotFoundException)
            { }

            try
            {
                apertureWidget = (GPhoto2.RadioCameraWidget)camera.GetSingleConfig("aperture");
            }
            catch (GPhoto2.WidgetNotFoundException)
            { }

            try
            {
                shutterSpeedWidget = (GPhoto2.RadioCameraWidget)camera.GetSingleConfig("shutterspeed");
            }
            catch (GPhoto2.WidgetNotFoundException)
            { }

            // TODO needs to be in some sort of `try` block for safety
            // Try to get a root widget
            rootWidget = camera.Config;
        }

        /// <summary>
        /// Ends the connection to the camera
        /// </summary>
        public void Exit()
        {
            // Only needs to be called if connected
            if (!connectionEstablished)
                return;

            // Cleanup widgets
            if (isoWidget != null)
            {
                isoWidget.Dispose();
                isoWidget = null;
            }

            if (apertureWidget != null)
            {
                apertureWidget.Dispose();
                apertureWidget = null;
            }

            if (shutterSpeedWidget != null)
            {
                shutterSpeedWidget.Dispose();
                shutterSpeedWidget = null;
            }

            // Close the actual connection
            camera.Exit();
            connectionEstablished = false;
        }


        #region Photo Capture Functions
        /// <summary>
        /// Grabs a preview photo from the camera.
        ///
        /// NOTE: At the moment, this only supports getting JPEG images from the camera.  In the future, things like
        ///       RAW will be supported
        /// </summary>
        /// <returns>The image data as a raw string of bytes.</returns>
        public byte[] PreviewAsBytes()
        {
            if (!connectionEstablished)
                throw new SharpCameraException("Camera connection needs to be established before capturing a preview.");

            // Get it
            return camera.CapturePreview();
        }

        /// <summary>
        /// Captures a full quality photo from the camera.
        ///
        /// NOTE: At the moment, this only supports getting JPEG images from the camera.  In the future, things like
        ///       RAW will be supported
        /// </summary>
        /// <returns>The image data as a raw string of bytes.</returns>
        public byte[] CaptureAsBytes()
        {
            if (!connectionEstablished)
                throw new SharpCameraException("Camera connection needs to be established before capturing an image.");

            // Get it
            return camera.Capture();
        }
        #endregion

        // These might change in the future how they're accessed.  I might give you an object instead in the futre
        // other than messing with these individual thingys
        #region Camera Config Functions
        #region ISO
        /// <summary>
        /// Does this camera have a configurable ISO setting?
        /// </summary>
        /// <value><c>true</c> if this instance has an ISO widget; otherwise, <c>false</c>.</value>
        public bool HasISO
        {
            get { return isoWidget != null; }
        }

        /// <summary>
        /// Gets a list of the possible ISO settings.
        /// </summary>
        /// <value>The ISO options.</value>
        public List<string> ISOOptions
        {
            get { return isoWidget.Choices; }
        }

        /// <summary>
        /// Gets or sets the ISO setting.
        /// </summary>
        /// <value>The ISO.</value>
        public string ISO
        {
            get { return isoWidget.Value; }
            set
            {
                isoWidget.Value = value;
                camera.SetSingleConfig(isoWidget);
            }
        }
        #endregion

        #region Aperture
        /// <summary>
        /// Does the camera have a configurable Aperture setting?
        /// </summary>
        /// <value><c>true</c> if this instance has an aperture widget; otherwise, <c>false</c>.</value>
        public bool HasAperture
        {
            get { return apertureWidget != null; }
        }

        /// <summary>
        /// Gets the possible aperture settings.
        /// </summary>
        /// <value>The aperture options.</value>
        public List<string> ApertureOptions
        {
            get { return apertureWidget.Choices; }
        }

        /// <summary>
        /// Gets or sets the aperture setting.
        /// </summary>
        /// <value>The aperture.</value>
        public string Aperture
        {
            get { return apertureWidget.Value; }
            set
            {
                apertureWidget.Value = value;
                camera.SetSingleConfig(apertureWidget);
            }
        }
        #endregion

        #region ShutterSpeed
        /// <summary>
        /// Does the camera have a configurable Shutter Speed setting?
        /// </summary>
        /// <value><c>true</c> if this instance has a shutter speed widget; otherwise, <c>false</c>.</value>
        public bool HasShutterSpeed
        {
            get { return shutterSpeedWidget != null; }
        }

        /// <summary>
        /// Gets the possible shutter speed settings.
        /// </summary>
        /// <value>The shutter speed options.</value>
        public List<string> ShutterSpeedOptions
        {
            get { return shutterSpeedWidget.Choices; }
        }

        /// <summary>
        /// Gets or sets the shutter speed setting.
        /// </summary>
        /// <value>The shutter speed.</value>
        public string ShutterSpeed
        {
            get { return shutterSpeedWidget.Value; }
            set
            {
                shutterSpeedWidget.Value = value;
                camera.SetSingleConfig(shutterSpeedWidget);
            }
        }
        #endregion
        #endregion

        /// <summary>
        /// Sees if the camera is still connected to the system.
        ///
        /// This function is blocking, and will (at most) hold for the amount of time set in the
        /// HeartbeatTimeout` property (by default is is 10 milliseconds).
        ///
        /// If it detected that the connection was lost, then this method will interally call the
        /// `Exit()` routine as to clean up any leftover resources.
        /// </summary>
        /// <value><c>true</c> if connected; otherwise, <c>false</c>.</value>
        public bool Connected
        {
            get
            {
                // Were we connected in the first place?
                if (!connectionEstablished)
                    return false;

                bool connected = false;
                try
                {
                    // Wait for a timeout
                    Tuple<GPhoto2.CameraEventType, IntPtr> res = camera.WaitForEvent((int)HeartbeatTimeout);
                    switch (res.Item1)
                    {
                        // If we get these, we sill should be connected (but we need to cleanup some memory)
                        case GPhoto2.CameraEventType.GP_EVENT_FILE_ADDED:
                        case GPhoto2.CameraEventType.GP_EVENT_FOLDER_ADDED:
                        case GPhoto2.CameraEventType.GP_EVENT_FILE_CHANGED:
                            Marshal.FreeHGlobal(res.Item2);
                            connected = true;
                            break;

                        // If we get these, we still should be connected
                        case GPhoto2.CameraEventType.GP_EVENT_TIMEOUT:
                        case GPhoto2.CameraEventType.GP_EVENT_UNKNOWN:
                        case GPhoto2.CameraEventType.GP_EVENT_CAPTURE_COMPLETE:
                            connected = true;
                            break;
                    }
                }
                catch (GPhoto2.GPhoto2Exception g2ex)
                {
                    // Check the error code
                    if (g2ex.ErrorCode == GPhoto2.GP_ERROR_IO_USB_FIND)
                    {
                        // The connection went bye-bye
                        connected = false;
                        Exit();
                    }
                    else
                        throw;      // Some other error, propgitate it forward
                }

                return connected;
            }
        }


        #region Static Data
        private static GPhoto2.GPContext scanContext = new GPhoto2.GPContext();
        #endregion

        #region Static Helper Methods
        /// <summary>
        /// Scans to see what cameras are connected (or not), and if we can take pictures with it.
        ///
        /// This will not enable the connection to the cameras.  You need to call the `Connect()` method to do that.
        /// </summary>
        public static List<TetheredCamera> Scan() {
            List<TetheredCamera> cameras = new List<TetheredCamera>();
            GPhoto2.GPPortInfoList portList = new GPhoto2.GPPortInfoList();
            GPhoto2.CameraAbilitiesList abilitiesList = new GPhoto2.CameraAbilitiesList();

            // Query
            portList.Load();
            abilitiesList.Load(scanContext);
            GPhoto2.CameraList camList = abilitiesList.Detect(portList);

            // See how many popped out
            for (int i = 0; i < camList.Count; i++)
            {
                string cameraName = camList.GetName(i);
                string usbPort = camList.GetValue(i);
                GPhoto2.CameraAbilities abilities = abilitiesList.GetAbilities(i);

                // We only want cameras that can take photos
                if (abilities.device_type == GPhoto2.GphotoDeviceType.GP_DEVICE_STILL_CAMERA)
                {
                    TetheredCamera cam = new TetheredCamera(cameraName, usbPort, portList);
                    cameras.Add(cam);
                }
            }

            return cameras;
        }


        /// <summary>
        /// If there is a set of cameras available, get the first one in the list.
        ///
        /// This will not enable the connection to the camera, you need to call the `Connect()` method to do that.
        /// </summary>
        /// <returns>The first readily available camera to use.  Returns `null` if there isn't one.</returns>
        public static TetheredCamera GetFirst()
        {
            List<TetheredCamera> cams = Scan();
            if (cams.Count > 0)
                return cams[0];
            else
                return null;
        }
        #endregion
    }
}
