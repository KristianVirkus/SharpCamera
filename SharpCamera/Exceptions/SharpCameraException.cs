﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  An exception that comes from the SharpCamera library.

using System;

namespace SharpCamera
{
    /// <summary>
    /// An exception from the SharpCamera library
    /// </summary>
    public class SharpCameraException : Exception
    {
        /// <summary>
        /// A SharpCamera exception.
        /// </summary>
        public SharpCameraException()
            : base()
        {
        }

        /// <summary>
        /// A SharpCamera exception, with a special message.
        /// </summary>
        /// <param name="message">Message.</param>
        public SharpCameraException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// A SharpCamera exception, with a message and an internal exception.
        /// </summary>
        /// <param name="message">Message.</param>
        /// <param name="inner">Inner exception.</param>
        public SharpCameraException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}

