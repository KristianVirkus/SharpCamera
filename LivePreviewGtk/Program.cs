// Author:       Benjamin N. Summerton <https://16bpp.net>
// License:      MIT

using System;
using Gtk;

namespace LivePreviewGtk
{
    class Program
    {
        [STAThread]
        public static void Main(string[] args)
        {
            Application.Init();

            Application app = new Application("net.sixteen_bpp.LivePreviewGtk", GLib.ApplicationFlags.None);
            app.Register(GLib.Cancellable.Current);

            LiveCameraPreviewWindow win = new LiveCameraPreviewWindow();
            app.AddWindow(win);

            win.Show();
            Application.Run();
        }
    }
}
