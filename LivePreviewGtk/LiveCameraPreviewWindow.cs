// Author:       Benjamin N. Summerton <https://16bpp.net>
// License:      MIT
// Description:  Connect to a camera and show a live feed.

using System;
using System.Timers;
using Gtk;
using UI = Gtk.Builder.ObjectAttribute;
using SharpCamera;

namespace LivePreviewGtk
{
    class LiveCameraPreviewWindow : Window
    {
        // Where the camera image will be shown
        [UI] private Image previewImage;

        private TetheredCamera cam;
        private object cameraLock;      // Thread syncing lock (Sytem.Timer objects run in their own threads)
        private Timer previewTimer;     // Timer for grabbing preview images
        private Timer scanTimer;        // Timer for scanning for Camera connections

        /// <summary>
        /// Creates a window to show a live camera feed.
        /// </summary>
        /// <returns></returns>
        public LiveCameraPreviewWindow() :
            this(new Builder("LiveCameraPreviewWindow.glade"))
        {
            cameraLock = new object();

            // Show previews
            previewTimer = new Timer(1000.0 / 30.0);       // 30 FPS
            previewTimer.Elapsed += displayPreview;

            // Create the scannner
            scanTimer = new Timer(1000.0 / 2.0);            // Scan 2x a second
            scanTimer.Elapsed += scanCameraConnections;
            scanTimer.Start();
        }

        private LiveCameraPreviewWindow(Builder builder) :
            base(builder.GetObject("LiveCameraPreviewWindow").Handle)
        {
            builder.Autoconnect(this);
        }

        #region GUI Signal Handlers
        /// <summary>
        /// Called when the window is closed.
        ///
        /// Will end the camera connection (if there is one) and end the application.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="a"></param>
        private void onDestroy(object sender, EventArgs a)
        {
            // Stop the timers
            scanTimer.Stop();
            previewTimer.Stop();

            lock (cameraLock)
            {
                // Close camera connection
                if (cam != null)
                    cam.Exit();
            }

            Application.Quit();
        }

        /// <summary>
        /// If the `ESC` key is pressed, this will close the application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void onKeyReleaseEvent(object sender, KeyReleaseEventArgs args)
        {
            if (args.Event.Key == Gdk.Key.Escape)
                Close();
        }
        #endregion  // GUI Signal Handlers

        #region Timer Callbacks
        /// <summary>
        /// Grabs an image from the camera (if there is one) and shows it.
        ///
        /// This is called about every 1/30th of a second.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void displayPreview(object sender, ElapsedEventArgs e)
        {
            // Get the preview image
            byte[] preview = null;
            lock (cameraLock)
            {
                if (cam != null)
                    preview = cam.PreviewAsBytes();
            }

            // Settting the image needs to turn in the GUI thread.
            Application.Invoke(delegate {

                // Show it and cleanup
                if (preview != null)
                    previewImage.Pixbuf = new Gdk.Pixbuf(preview);
            });
        }

        /// <summary>
        /// Checks for active camera connections.
        ///
        /// This is called about twice a second.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void scanCameraConnections(object sender, ElapsedEventArgs e)
        {
            bool camConnected = false;
            lock (cameraLock)
                camConnected = cam != null;

            if (!camConnected)
            {
                // (Try to) find a new one
                lock (cameraLock)
                {
                    cam = TetheredCamera.GetFirst();
                    cam.Connect();
                }
                previewTimer.Start();
            }
            else
            {
                // See if the current one is still there
                bool stillConnected = false;
                lock (cameraLock)
                    stillConnected = cam.Connected;

                if (!stillConnected)
                {
                    previewTimer.Stop();

                    // Clear it out
                    lock (cameraLock)
                    {
                        cam.Exit();
                        cam = null;
                    }
                }
            }
        }
        #endregion  // TimerCallbacks
    }
}